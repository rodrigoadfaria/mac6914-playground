#!/bin/bash

operator="$1"
size_set="1"
echo "Test: operator $operator | dataset size $size_set"
trios_test $operator/trop-$size_set-$operator ../dataset/TRIOSTest-noise/test.set

size_set="2"
echo "Test: operator $operator | dataset size $size_set"
trios_test $operator/trop-$size_set-$operator ../dataset/TRIOSTest-noise/test.set

size_set="5"
echo "Test: operator $operator | dataset size $size_set"
trios_test $operator/trop-$size_set-$operator ../dataset/TRIOSTest-noise/test.set

size_set="10"
echo "Test: operator $operator | dataset size $size_set"
trios_test $operator/trop-$size_set-$operator ../dataset/TRIOSTest-noise/test.set

size_set="15"
echo "Test: operator $operator | dataset size $size_set"
trios_test $operator/trop-$size_set-$operator ../dataset/TRIOSTest-noise/test.set

size_set="20"
echo "Test: operator $operator | dataset size $size_set"
trios_test $operator/trop-$size_set-$operator ../dataset/TRIOSTest-noise/test.set

size_set="25"
echo "Test: operator $operator | dataset size $size_set"
trios_test $operator/trop-$size_set-$operator ../dataset/TRIOSTest-noise/test.set
