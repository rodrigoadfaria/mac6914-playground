#!/bin/bash

operator="$1"
training_set="training.set"

if [ "$2" != "" ] && [ "$2" != 1 ]; then
    training_set="training"$2".set"
fi

echo "Building for $operator at training set $training_set"

trios_build single BB $operator/$operator ../dataset/TRIOSTest-character/$training_set $operator/trop-$2-$operator
